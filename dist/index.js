"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var DatePickerCalendarComponent = /** @class */ (function (_super) {
    __extends(DatePickerCalendarComponent, _super);
    function DatePickerCalendarComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            val: props.value ? props.value : new Date(),
            label: '',
            calendar: {},
            weekDaysData: [],
            daysData: []
        };
        _this.nodeRef = React.createRef();
        _this.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            _this.weekDays = [{
                    name: 'Monday',
                    shortName: 'Mon'
                }, {
                    name: 'Tuesday',
                    shortName: 'Tue'
                }, {
                    name: 'Wednesday',
                    shortName: 'Wed'
                }, {
                    name: 'Thursday',
                    shortName: 'Thu'
                }, {
                    name: 'Friday',
                    shortName: 'Fri'
                }, {
                    name: 'Saturday',
                    shortName: 'Sat'
                }, {
                    name: 'Sunday',
                    shortName: 'Sun'
                }];
        _this.selectedDate = new Date(props.value).getDate();
        _this.selectedMonth = null;
        _this.selectedYear = null;
        return _this;
    }
    DatePickerCalendarComponent.prototype.componentDidMount = function () {
        document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false);
        document.addEventListener('mousedown', this.handleMouseDown.bind(this), false);
        var weekDays = [];
        for (var i = 0; i < this.weekDays.length; i++) {
            weekDays.push(React.createElement("td", { key: i }, this.weekDays[i].shortName));
        }
        this.selectedMonth = new Date(this.state.val).getMonth();
        this.selectedYear = new Date(this.state.val).getFullYear();
        this.setState({
            weekDaysData: weekDays,
            daysData: this.createCal(new Date(this.state.val).getFullYear(), new Date(this.state.val).getMonth())
        });
    };
    DatePickerCalendarComponent.prototype.componentWillUnmount = function () {
        document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false);
    };
    DatePickerCalendarComponent.prototype.handleMouseDown = function (e) {
        if (this.nodeRef.current === null) {
            return;
        }
        if (this.nodeRef.current.contains(e.target)) {
            return;
        }
        this.handleClickOutside();
    };
    DatePickerCalendarComponent.prototype.handleClickOutside = function () {
        this.props.clickedOutside ? this.props.clickedOutside() : false;
    };
    DatePickerCalendarComponent.prototype.createCal = function (year, month) {
        var _this = this;
        var todayDate = new Date().getDate();
        var todayMonth = new Date().getMonth();
        var todayYear = new Date().getFullYear();
        var day = 1, i, j, haveDays = true, startDay = new Date(year, month, day).getDay(), daysInMonth = [31, (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], calendar = [];
        i = 0;
        if (startDay === 0) {
            startDay = 6;
        }
        else {
            startDay -= 1;
        }
        while (haveDays) {
            calendar[i] = [];
            for (j = 0; j < 7; j++) {
                if (i === 0) {
                    if (j >= startDay) {
                        calendar[i][j] = day++;
                    }
                }
                else if (day <= daysInMonth[month] && i > 0) {
                    calendar[i][j] = day++;
                }
                else {
                    calendar[i][j] = "";
                    haveDays = false;
                }
                if (day > daysInMonth[month]) {
                    haveDays = false;
                }
            }
            i++;
        }
        for (var k = 0; k < calendar.length; k++) {
            var tdEl = [];
            for (var z = 0; z < calendar[k].length; z++) {
                var classesToAdd = '';
                if (parseInt(this.selectedDate) === calendar[k][z]) {
                    classesToAdd += ' selected';
                }
                else if (todayDate === calendar[k][z] && todayMonth === month && todayYear === year) {
                    classesToAdd += ' today';
                }
                else if (parseInt(this.selectedDate) === calendar[k][z]) {
                    classesToAdd += ' selected';
                }
                tdEl.push(React.createElement('td', { key: z, className: classesToAdd, onClick: function (e) { _this.dateClicked(e); } }, calendar[k][z]));
            }
            calendar[k] = React.createElement('tr', { key: k }, tdEl);
        }
        return calendar;
    };
    DatePickerCalendarComponent.prototype.dateClicked = function (e) {
        var newDate = parseInt(e.currentTarget.textContent);
        this.selectedDate = newDate;
        var weekDays = [];
        for (var i = 0; i < this.weekDays.length; i++) {
            weekDays.push(React.createElement("td", { key: i }, this.weekDays[i].shortName));
        }
        var value = new Date(this.selectedYear, this.selectedMonth, this.selectedDate);
        this.setState({
            val: value,
            weekDaysData: weekDays,
            daysData: this.createCal(new Date(this.state.val).getFullYear(), new Date(this.state.val).getMonth())
        });
        this.props.onChange ? this.props.onChange(value) : false;
    };
    DatePickerCalendarComponent.prototype.nextMonth = function () {
        var newDate = new Date(this.state.val.setMonth(this.state.val.getMonth() + 1));
        var dateF = new Date(newDate);
        this.selectedDate = null;
        this.selectedMonth = new Date(newDate).getMonth();
        this.selectedYear = new Date(newDate).getFullYear();
        var weekDays = [];
        for (var i = 0; i < this.weekDays.length; i++) {
            weekDays.push(React.createElement("td", { key: i }, this.weekDays[i].shortName));
        }
        this.setState({
            val: dateF,
            weekDaysData: weekDays,
            daysData: this.createCal(new Date(newDate).getFullYear(), new Date(newDate).getMonth())
        });
    };
    DatePickerCalendarComponent.prototype.prevMonth = function () {
        var newDate = new Date(this.state.val.setMonth(this.state.val.getMonth() - 1));
        var dateF = new Date(newDate);
        this.selectedDate = null;
        var weekDays = [];
        this.selectedMonth = new Date(newDate).getMonth();
        this.selectedYear = new Date(newDate).getFullYear();
        for (var i = 0; i < this.weekDays.length; i++) {
            weekDays.push(React.createElement("td", { key: i }, this.weekDays[i].shortName));
        }
        this.setState({
            val: dateF,
            weekDaysData: weekDays,
            daysData: this.createCal(new Date(newDate).getFullYear(), new Date(newDate).getMonth())
        });
    };
    DatePickerCalendarComponent.prototype.getMonthName = function () {
        var month = new Date(this.state.val).getMonth();
        return this.months[month];
    };
    DatePickerCalendarComponent.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", { ref: this.nodeRef },
            React.createElement("div", { id: "cal" },
                React.createElement("div", { className: "header" },
                    React.createElement("span", { className: "left button", id: "prev", onClick: function () { _this.prevMonth(); } }, " \u2329 "),
                    React.createElement("span", { className: "left hook" }),
                    React.createElement("span", { className: "month-year", id: "label" },
                        " ",
                        this.getMonthName(),
                        " ",
                        new Date(this.state.val).getFullYear(),
                        " "),
                    React.createElement("span", { className: "right hook" }),
                    React.createElement("span", { className: "right button", id: "next", onClick: function () { _this.nextMonth(); } }, " \u232A ")),
                React.createElement("table", { id: "days" },
                    React.createElement("tbody", null,
                        React.createElement("tr", null, this.state.weekDaysData))),
                React.createElement("div", { id: "cal-frame" },
                    React.createElement("table", { id: "cal-frame" },
                        React.createElement("tbody", null, this.state.daysData))))));
    };
    return DatePickerCalendarComponent;
}(React.Component));
exports.default = DatePickerCalendarComponent;
//# sourceMappingURL=index.js.map