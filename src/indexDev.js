import * as React from 'react';
export default class DatePickerCalendarComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      val: props.value ? props.value : new Date(),
      label: '',
      calendar: {},
      weekDaysData:[],
      daysData:[]
    };
    this.nodeRef= React.createRef();
    this.months = ["January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December"],
    this.weekDays = [{
      name: 'Monday',
      shortName: 'Mon'
    },{
      name: 'Tuesday',
      shortName: 'Tue'
    },{
      name: 'Wednesday',
      shortName: 'Wed'
    },{
      name: 'Thursday',
      shortName: 'Thu'
    },{
      name: 'Friday',
      shortName: 'Fri'
    },{
      name: 'Saturday',
      shortName: 'Sat'
    },{
      name: 'Sunday',
      shortName: 'Sun'
    }];
    this.selectedDate = new Date(props.value).getDate();
    this.selectedMonth = null;
    this.selectedYear = null;
  }
  componentDidMount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
    document.addEventListener('mousedown', this.handleMouseDown.bind(this), false)
    let weekDays = [];
    for(let i=0; i < this.weekDays.length; i++){
      weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
    }
    this.selectedMonth = new Date(this.state.val).getMonth();
    this.selectedYear = new Date(this.state.val).getFullYear();
    this.setState({
      weekDaysData: weekDays,
      daysData: this.createCal(new Date(this.state.val).getFullYear(), new Date(this.state.val).getMonth())
    });
  }
  componentWillUnmount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  handleMouseDown(e){
    if(this.nodeRef.current === null){
      return;
    }
    if(this.nodeRef.current.contains(e.target)){
      return;
    }
    this.handleClickOutside()
  }
  handleClickOutside(){
    this.props.clickedOutside ? this.props.clickedOutside() : false;
  }
  createCal(year, month) {
    let todayDate = new Date().getDate();
    let todayMonth = new Date().getMonth();
    let todayYear = new Date().getFullYear();
    let day = 1,
    i,
    j,
    haveDays = true,
    startDay = new Date(year, month, day).getDay(),
    daysInMonth = [31, (((year%4===0)&&(year%100!==0))||(year%400===0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ],
    calendar = [];
    i = 0;
    if(startDay === 0){
      startDay = 6;
    } else {
      startDay -= 1;
    }
    while(haveDays) {
      calendar[i] = [];
      for (j = 0; j < 7; j++) {
        if(i === 0){
          if(j >= startDay) {
            calendar[i][j] = day++
          }
        }
        else if(day <= daysInMonth[month] && i > 0){
          calendar[i][j] = day++
        }  else {
          calendar[i][j] = "";
          haveDays = false;
        }
        if(day > daysInMonth[month]){
          haveDays = false;
        }
      }
      i++;
    }
    for (let k = 0; k < calendar.length; k++) {
      let tdEl = [];
      for (let z = 0; z < calendar[k].length; z++) {
        let classesToAdd = '';
        if(parseInt(this.selectedDate) === calendar[k][z]){
          classesToAdd+=' selected';
        } else if(todayDate === calendar[k][z] && todayMonth === month && todayYear === year){
          classesToAdd+=' today';
        } else if(parseInt(this.selectedDate) === calendar[k][z]){
          classesToAdd+=' selected';
        }
        tdEl.push(React.createElement('td', {key: z, className: classesToAdd, onClick: (e) => {this.dateClicked(e)} }, calendar[k][z]));
      }
      calendar[k] = React.createElement('tr', {key: k}, tdEl);
    }
    return calendar;
  }
  dateClicked(e){
    let newDate = parseInt(e.currentTarget.textContent);
    this.selectedDate = newDate;
    let weekDays = [];
    for(let i=0; i < this.weekDays.length; i++){
      weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
    }
    let value =  new Date(this.selectedYear, this.selectedMonth, this.selectedDate);
    this.setState({
      val: value,
      weekDaysData: weekDays,
      daysData: this.createCal(new Date(this.state.val).getFullYear(), new Date(this.state.val).getMonth())
    });
    this.props.onChange ? this.props.onChange(value) : false;
  }
  nextMonth(){
    let newDate = new Date(this.state.val.setMonth(this.state.val.getMonth() + 1));
    let dateF = new Date(newDate);
    this.selectedDate = null;
    this.selectedMonth = new Date(newDate).getMonth();
    this.selectedYear = new Date(newDate).getFullYear();
    let weekDays = [];
    for(let i=0; i < this.weekDays.length; i++){
      weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
    }
    this.setState({
      val: dateF,
      weekDaysData: weekDays,
      daysData: this.createCal(new Date(newDate).getFullYear(), new Date(newDate).getMonth())
    });
  }
  prevMonth(){
    let newDate =  new Date(this.state.val.setMonth(this.state.val.getMonth()-1))
    let dateF = new Date(newDate);
    this.selectedDate = null;
    let weekDays = [];
    this.selectedMonth = new Date(newDate).getMonth();
    this.selectedYear = new Date(newDate).getFullYear();
    for(let i=0; i < this.weekDays.length; i++){
      weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
    }
    this.setState({
      val: dateF,
      weekDaysData: weekDays,
      daysData: this.createCal(new Date(newDate).getFullYear(), new Date(newDate).getMonth())
    });
  }
  getMonthName(){
    let month = new Date(this.state.val).getMonth();
    return this.months[month];
  }
  render(){
    return(
    <div ref={this.nodeRef}>
  <div id="cal">
    <div className={"header"}>
    <span className={"left button"} id="prev" onClick={()=>{this.prevMonth()}}> &lang; </span>
    <span className={"left hook"}></span>
    <span className={"month-year"} id="label"> {this.getMonthName()} {new Date(this.state.val).getFullYear()} </span>
    <span className={"right hook"}></span>
    <span className={"right button"} id="next" onClick={()=>{this.nextMonth()}}> &rang; </span>
    </div>
    <table id="days">
    <tbody>
    <tr>
    {this.state.weekDaysData}
  </tr>
    </tbody>
    </table>
    <div id="cal-frame">
    <table id="cal-frame">
    <tbody>
    {this.state.daysData}
  </tbody>
    </table>
    </div>
    </div>
    </div>
  )
  }
}
