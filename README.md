# react-simple-date-picker

> ReactJs simple calendar date picker component

[![NPM](https://img.shields.io/npm/v/react-simple-calendar.svg)](https://www.npmjs.com/package/react-simple-calendar) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm i react-simple-calendar-date-picker --save
```
## GitLab
https://gitlab.com/damjan89/react-simple-date-picker
## Preview
![Date-Calendar](https://gitlab.com/damjan89/react-simple-date-picker/raw/master/calendar-preview.png)
## Usage
React >= 16.9.0
```tsx
import * as React from 'react'

import DatePickerCalendarComponent from 'react-simple-calendar-date-picker';
import 'react-simple-calendar-date-picker/src/assets/style.css';
class IndexComponent extends React.Component {
   constructor(){
    super();
    this.state = {
        dateValue: new Date()
    }
   }
/*
  * Name: ClickedOutside
  * Description: This function is called when user clicks outside calendar borders (useful when calendar is in popup)
  * Parameters: event
  * author: Nick Dam
  * */
  clickedOutside(event){
    //if calendar is in popup you can hide it here
  }
  /*
  * Name: valueChanged
  * Description: This function is called when user choose a date
  * Parameters: value -> Clicked Date
  * author: Nick Dam
  * */
  valueChanged(value){
    this.setState({
      dateValue: value
    });
  }
  render () {
    return (
        <div>
          <p>{this.state.dateValue.toString()}</p>
          <DatePickerCalendarComponent
                    value={this.state.dateValue}
                    clickedOutside={(e)=>this.clickedOutside(e)}
                    onChange={(val)=>this.valueChanged(val)}/>
        </div>
    )
  }
}
```

## License

MIT © [Nick Dam](https://gitlab.com/damjan89)
